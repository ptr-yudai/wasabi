# coding: utf-8
from datetime import datetime
from wasabi.core.database import db

"""
lecturesテーブルの定義
"""
class Lectures(db.Model):
    __tablename__ = 'lecture'
    # 識別番号 Integer
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # 講義番号 Integer
    cid = db.Column(db.Integer, nullable=False)
    # 公開開始日時 DateTime
    start = db.Column(db.DateTime, nullable=False, default=datetime.now)
    # 公開終了日時 DateTime
    end = db.Column(db.DateTime, nullable=True, default=datetime.now)
    # 講義資料パス String
    lectpath = db.Column(db.String(256), nullable=True)
    # 講義資料説明 Text
    lect = db.Column(db.Text, nullable=True)
    # 課題資料パス String
    taskpath = db.Column(db.String(256), nullable=True)
    # 課題資料説明 Text
    task = db.Column(db.Text, nullable=True)
    # 課題があるか Boolean
    withtask = db.Column(db.Boolean, nullable=False, default=False)
    # 課題提出者 Text
    submitted = db.Column(db.Text, nullable=False, default='')
    # 作成，更新時刻 DateTime
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
