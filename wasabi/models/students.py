# coding: utf-8
from datetime import datetime
from flask_login import UserMixin
from wasabi.core.database import db

"""
studentsテーブルの定義
"""
class Students(UserMixin, db.Model):
    __tablename__ = 'students'
    # 識別番号 Integer
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # 学籍番号 String
    sid = db.Column(db.String(8), nullable=False)
    # パスワード String(128)
    password = db.Column(db.String(128), nullable=False)
    # 学科 String(8)
    department = db.Column(db.String(8), nullable=False)
    # 学年 Integer
    grade = db.Column(db.Integer, nullable=False)
    # 番号 Integer
    number = db.Column(db.Integer, nullable=False)
    # 氏名 String(64)
    name = db.Column(db.String(64), nullable=False)
    # 作成，更新時刻 DateTime
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

