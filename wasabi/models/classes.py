# coding: utf-8
from datetime import datetime
from wasabi.core.database import db

"""
classesテーブルの定義
"""
class Classes(db.Model):
    __tablename__ = 'classes'
    # 識別番号 Integer
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # 科目名 String(64)
    name = db.Column(db.String(64), nullable=False)
    # 曜日 String(32)
    day = db.Column(db.String(32), nullable=False)
    # 時限
    period = db.Column(db.Text, nullable=False)
    # 対象学科 String(8)
    department = db.Column(db.String(16), nullable=False)
    # 対象学年 Integer
    grade = db.Column(db.Integer, nullable=False)
    # 単位数 Integer
    module = db.Column(db.Integer, nullable=False)
    # 担当者 String(128)
    teacher = db.Column(db.Text, nullable=False)
    # 選択か必修か Boolean
    optional = db.Column(db.Boolean, nullable=False, default=False)
    # 作成，更新時刻 DateTime
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

