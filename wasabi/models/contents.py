# coding: utf-8
from datetime import datetime
from wasabi.core.database import db

"""
contentsテーブルの定義
"""
class Contents(db.Model):
    __tablename__ = 'contents'
    # 識別番号 Integer
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    # 科目名 String
    name = db.Column(db.String(64), nullable=False)
    # 名前 String
    subtitle = db.Column(db.String(64), nullable=False, default="")
    # 開始日時 DateTime
    start = db.Column(db.DateTime, nullable=False, default=datetime.now)
    # 終了日時 DateTime
    end = db.Column(db.DateTime, nullable=True, default=datetime.now)
    # 出席パスワード String
    password = db.Column(db.String(64), nullable=True)
    # 出席受付期間(分) Integer
    limit = db.Column(db.Integer, nullable=False, default=15)
    # 受講者リスト
    attendee = db.Column(db.Text, nullable=True)
    # 講義資料および課題
    
    # 作成，更新時刻 DateTime
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
