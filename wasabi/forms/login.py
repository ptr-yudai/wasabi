# coding: utf-8
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, validators

class LoginForm(FlaskForm):
    sid = StringField(u'学籍番号', [
        validators.Length(min=1, max=5)
    ])
    password = PasswordField(u'パスワード', [
        validators.Length(min=1, max=128)
    ])
    submit = SubmitField(u'ログイン')
