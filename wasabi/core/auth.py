# coding: utf-8
from flask_login import LoginManager
from wasabi.models.students import Students

login_manager = LoginManager()

@login_manager.user_loader
def load_user(user_id):
    return Students.query.get(user_id)

def init_login(app):
    login_manager.login_view = '/login'
    login_manager.login_message = u"先にログインしてください"
    login_manager.init_app(app)
