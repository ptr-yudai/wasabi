# coding: utf-8
from flask_session import Session

def init_session(app):
    sess = Session()
    sess.init_app(app)
