# coding: utf-8
from flask_wtf.csrf import CSRFProtect

def init_csrf(app):
    csrf = CSRFProtect(app)
    csrf.init_app(app)
