# coding: utf-8
from wasabi.core.database import db
from wasabi.models.students import Students
from wasabi.models.classes import Classes
from wasabi.models.contents import Contents
from wasabi.models.lectures import Lectures

from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

def init_admin(app):
    admin = Admin(app, name='wasabi')
    admin.add_view(ModelView(Students, db.session))
    admin.add_view(ModelView(Classes, db.session))
    admin.add_view(ModelView(Contents, db.session))
    admin.add_view(ModelView(Lectures, db.session))
    return admin
