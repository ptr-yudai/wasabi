import flask
import json

app = flask.Flask(__name__)
try:
    f = open('config.json', 'r')
    config = json.load(f)
    f.close()
except IOError:
    print("[FATAL] Configuration file is missing.")
    exit(1)
except ValueError:
    print("[FATAL] Configuration file is broken. Check the json format.")
    exit(1)
