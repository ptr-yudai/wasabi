# coding: utf-8
from flask import render_template
from wasabi import app, config

@app.route("/")
def Index():
    return render_template(
        "index.html",
        config = config
    )
