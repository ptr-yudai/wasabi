# coding: utf-8
from flask import render_template
from wasabi import app, config
from wasabi.interface.Class import utility_processor

from flask import request


@app.route("/query", methods=['POST'])
def Query():
    util = utility_processor()
    if 'sid' in request.form and 'password' in request.form and 'class' in request.form:
        sid = request.form['sid']
        password = request.form['password']
        cls = request.form['class']
        if util['on_attend'](sid, cls):
            return "ok"
        if util['is_attendable'](cls):
            if util['try_attend'](sid, cls, password):
                # 出席完了
                return "ok"
            else:
                # パスワード間違い
                return "wrong"
        else:
            # 出席受付時間外
            return "sorry"
    # 不明なリクエスト
    return "error"
