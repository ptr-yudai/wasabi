# coding: utf-8
from wasabi import app, config
from wasabi.core.database import db
from wasabi.models.contents import Contents
from wasabi.models.lectures import Lectures

import os
from flask import redirect, render_template, url_for, flash, request
from flask_login import login_required, current_user
from werkzeug import secure_filename

@app.context_processor
def utility_processor():
    # 内容取得
    def get_lecture(cid):
        contents = Contents.query.get(cid)
        return contents
    
    return dict(get_lecture=get_lecture)

def is_submited(cid, sid):
    # 課題を提出済みか
    lecture = Lectures.query.get(cid)
    submitted_list = lecture.submitted.split(',')
    for submitted in submitted_list:
        if submitted == sid:
            return True
    return False

@app.route('/lecture/<id>', methods=['GET', 'POST'])
@login_required
def Lecture(id):
    # 講義を取得
    lecture = Lectures.query.get(id)
    if lecture is None:
        flash(u'選択された講義が見つかりません')
        return redirect(url_for('Class'))
    # 課題を提出
    if request.method == 'POST' and 'upfile' in request.files:
        upload = request.files['upfile']
        if upload:
            filename = secure_filename(upload.filename)
            upload.save(os.path.join("../static/uploads/", + filename))
            # 提出済みとして登録
            if not is_submitted(id, current_user.sid):
                Lectures.query.filter_by(cid=id).update(dict(submitted=lecture.submitted + ',' + current_user.sid))
                db.session.commit()

    return render_template(
        'lecture.html',
        config = config,
        lecture = lecture
    )
