# coding: utf-8
from wasabi import app, config
from wasabi.forms.login import LoginForm
from wasabi.models.students import Students

from flask import redirect, render_template, request, url_for, flash, get_flashed_messages
from flask_login import login_user

import hashlib

@app.route("/login", methods=['GET', 'POST'])
def Login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        # ログイン要求
        user = Students.query.filter_by(
            sid=form.sid.data,
            password=hashlib.sha256(form.password.data).hexdigest()
        ).first()
        if user:
            if login_user(user):
                # 認証成功
                return redirect(url_for('Class'))
        flash(u'学籍番号かパスワードが違います')
    return render_template(
        "login.html",
        form = form,
        config = config
    )
