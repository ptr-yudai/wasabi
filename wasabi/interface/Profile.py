# coding: utf-8
from flask import redirect, render_template, request, url_for
from wasabi import app, config

@app.route("/profile")
def Profile():
    return render_template(
        "profile.html",
        config = config
    )

