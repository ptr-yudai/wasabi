# coding: utf-8
from wasabi import app, config
from wasabi.core.database import db
from wasabi.models.classes import Classes
from wasabi.models.contents import Contents

from flask import render_template
from flask_login import login_required
from sqlalchemy import desc
import datetime

@app.context_processor
def utility_processor():
    # 授業内容取得
    def get_contents(name):
        contents = Contents.query.filter_by(name=name).order_by(Contents.start).all()
        return contents
    # 今日の曜日を番号で取得
    def get_day():
        return datetime.datetime.today().weekday()
    # 講義の出席状況を取得
    def get_attend_list(sid, name):
        contents = Contents.query.filter_by(name=name).order_by(Contents.start).all()
        attend_list = {}
        for cls in contents:
            attendees = cls.attendee.split(',')
            attend_list[cls.start] = False
            for attendee in attendees:
                if attendee == sid:
                    attend_list[cls.start] = True
        return attend_list
    # 講義の出席率を取得
    def get_attend_rate(attend_list):
        return float(attend_list.values().count(True)) / len(attend_list)
    
    return dict(get_contents=get_contents,
                get_day=get_day,
                get_attend_rate=get_attend_rate,
                get_attend_list=get_attend_list)

@app.route('/status-submit')
@login_required
def Submit():
    # 曜日ごとに科目を整列
    classes = []
    for day in ['Mon', 'Tue', 'Wed', 'Thu', 'Fri']:
        classes.append(Classes.query.filter_by(day=day).order_by(Classes.period).all())

    return render_template(
        'status-submit.html',
        config = config,
        classes = classes
    )
