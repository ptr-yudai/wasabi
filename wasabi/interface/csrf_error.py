# coding: utf-8
from wasabi import app, config
from flask import render_template
from flask_wtf.csrf import CSRFError

@app.errorhandler(CSRFError)
def csrf_error(e):
    return render_template(
        "csrf_error.html",
        config = config,
        reason = e.description
    ), 400
