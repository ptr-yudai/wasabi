# coding: utf-8
from wasabi import app, config
from wasabi.core.database import db
from wasabi.models.contents import Contents
from wasabi.models.classes import Classes

from flask import render_template
from flask_login import login_required
from sqlalchemy import desc
import datetime

@app.context_processor
def utility_processor():
    # 授業内容取得
    def get_contents(name):
        contents = Contents.query.filter_by(name=name).order_by(Contents.start).all()
        return contents
    # 今日の曜日を番号で取得
    def get_day():
        return datetime.datetime.today().weekday()
    # 授業中か
    def is_open(name):
        class_list = Contents.query.filter_by(name=name).all()
        for cls in class_list:
            start, end, limit = cls.start, cls.end, cls.limit
            now = datetime.datetime.now()
            if start <= now <= end:
                return True
        return False
    # 出席受付中か
    def is_attendable(name):
        class_list = Contents.query.filter_by(name=name).all()
        for cls in class_list:
            start, end, limit = cls.start, cls.end, cls.limit
            now = datetime.datetime.now()
            if start <= now <= start + datetime.timedelta(minutes = limit):
                return True
        return False
    # 出席済みか
    def on_attend(sid, name):
        class_list = Contents.query.filter_by(name=name).all()
        for cls in class_list:
            start, end, limit = cls.start, cls.end, cls.limit
            now = datetime.datetime.now()
            if start <= now <= end:
                for attendee in cls.attendee.split(','):
                    if attendee == sid:
                        return True
                return False
        return False
    
    # 出席パスワードを確認
    def try_attend(sid, name, password):
        class_list = Contents.query.filter_by(name=name).all()
        for cls in class_list:
            start, end, limit = cls.start, cls.end, cls.limit
            now = datetime.datetime.now()
            if start <= now <= start + datetime.timedelta(minutes = limit):
                if cls.password == password:
                    # 出席登録
                    Contents.query.filter_by(id=cls.id).update(dict(attendee=cls.attendee + ',' + sid))
                    db.session.commit()
                    return True
                else:
                    return False
        return False

    return dict(get_contents=get_contents,
                get_day=get_day,
                is_open=is_open,
                is_attendable=is_attendable,
                try_attend=try_attend,
                on_attend=on_attend)

@app.route('/class')
@login_required
def Class():
    # 曜日ごとに科目を整列
    classes = []
    for day in ['Mon', 'Tue', 'Wed', 'Thu', 'Fri']:
        classes.append(Classes.query.filter_by(day=day).order_by(Classes.period).all())
    
    return render_template(
        'class.html',
        config = config,
        classes = classes
    )
