# coding: utf-8
from wasabi import app, config
from wasabi.core.database import db

from flask import render_template
from flask_login import login_required

@app.route('/add-class')
#@login_required
def AddClass():
    return render_template(
        'add-class.html',
        config = config
    )
