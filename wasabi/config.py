# coding: utf-8
import os
from wasabi import config

"""
Flask Configuration
"""
class Config(object):
    # Debug Mode
    DEBUG = config['server']['debug']
    TESTING = config['server']['testing']
    # Server Setting
    SERVER_NAME = config['server']['host'] + ':' + str(config['server']['port'])
    # SQL Config
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{user}:{pass}@{host}/wasabi?charset=utf8'.format(**{
        'user': config['database']['user'],
        'pass': config['database']['pass'],
        'host': config['database']['host']
    })
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    # Admin Dashboard
    FLASK_ADMIN_SWATCH = 'cerulean'
    # CSRF
    WTF_CSRF_SECRET_KEY = os.urandom(24)
    # Session
    SECRET_KEY = os.urandom(24)
    SESSION_COOKIE_DOMAIN = SERVER_NAME
