#!/usr/bin/env python
# coding: utf-8
import os
from wasabi import app
from wasabi.core.admin import init_admin
from wasabi.core.auth import init_login
from wasabi.core.csrf import init_csrf
from wasabi.core.database import init_db
from wasabi.core.session import init_session
import wasabi.models

from wasabi.interface.csrf_error import csrf_error
from wasabi.interface.Index import Index
from wasabi.interface.Class import Class
from wasabi.interface.Attend import Attend
from wasabi.interface.Submit import Submit
from wasabi.interface.Lecture import Lecture
from wasabi.interface.Profile import Profile
from wasabi.interface.Login import Login
from wasabi.interface.Logout import Logout
from wasabi.interface.Query import Query

app.secret_key = os.urandom(24)
app.config.from_object('wasabi.config.Config')
migrate = init_db(app)
admin = init_admin(app)
init_csrf(app)
login_manager = init_login(app)

if __name__ == '__main__':
    app.run()
    
